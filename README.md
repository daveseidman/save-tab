# Save Tab

Interactive WebGL website for the campaign to save Tab soda from Coke's plan to discontinue it.  

Users can claim a can by filling out their name, location and reason for wanting Coke to keep


## Development

create a .env file in the root of the project with this line:
```
PORT=8000
```

run `npm start` to run the server and client in one terminal or   
`npm run client-dev`  and
`npm run server-dev`  in separate terminals.


## TODO
- Fix Audio (sounds echoey)
- can get new can to appear (sign your can) offscreen, check tweens
- on mobile, sign your can, submit button stays active after clearing signature pad
- Test periods or special characters in input fields

nice to haves:
- disable clicking on cans while they're still pouring in?
- Routing (almost complete)
- play with restitution / friction / bounciness
- Resize occsionally sticks, debouce may fix
- test a resize during camera tween
- Prevent user from submitting multiple times (recaptcha may be doing this already)
- Search function (input with fuzzy matching for names / locations / reasons)
- split stylesheets and clean up
- Server side, use Path.join() for x os compatibility
