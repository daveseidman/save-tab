import autoBind from 'auto-bind';
import onChange from 'on-change';

export default class SoundEffects {
  constructor() {
    autoBind(this);
    this.audioContext = new AudioContext();
    this.volume = 0;
    this.ready = false;
  }

  play() {
    const source = this.audioContext.createBufferSource();
    source.buffer = this.audioBuffers[Math.floor(Math.random() * this.audioBuffers.length)];
    source.connect(this.volumeNode);
    source.start();
  }

  loadMP3(URL) {
    return new Promise((resolve) => {
      fetch(URL).then(res => res.arrayBuffer())
        .then(arrayBuffer => this.audioContext.decodeAudioData(arrayBuffer))
        .then(audioBuffer => resolve(audioBuffer));
    });
  }

  load() {
    this.volumeNode = this.audioContext.createGain();
    this.volumeNode.gain.value = 0;
    this.volumeNode.connect(this.audioContext.destination);

    const audioFiles = ['../assets/audio/can1.mp3', '../assets/audio/can2.mp3', '../assets/audio/can3.mp3', '../assets/audio/can4.mp3', '../assets/audio/can5.mp3'];
    // const audioFiles = ['../assets/audio/tik.mp3', '../assets/audio/tok.mp3'];
    const loaders = [];
    for (let i = 0; i < audioFiles.length; i += 1) {
      loaders.push(this.loadMP3(audioFiles[i]));
    }
    Promise.all(loaders).then((buffers) => {
      this.audioBuffers = buffers;
      this.ready = true;
    });
  }
}
