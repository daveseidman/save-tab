import { createEl } from 'lmnt';
import { CanvasTexture } from 'three';
import autoBind from 'auto-bind';
import { wrapText } from './utils';

const textSize = 120;

export default class Label {
  constructor() {
    autoBind(this);
    this.fullCanvas = createEl('canvas', { width: 1024, height: 2048 });
    this.fullContext = this.fullCanvas.getContext('2d');
    this.fullContext.font = `bold ${textSize}px Kanit`;
    this.fullContext.fillRect(0, 0, 1024, 2048);
    this.canvasTexture = new CanvasTexture(this.fullCanvas);
  }

  clear() {
    this.fullContext.fillStyle = 'black';
    this.fullContext.fillRect(0, 0, 1024, 1024);
    this.canvasTexture.needsUpdate = true;
  }

  updateText(event) {
    const target = event.target || event;
    const offsetY = parseInt(target.getAttribute('offsetY'), 10);
    this.fullContext.fillStyle = 'black';
    this.fullContext.fillRect(0, offsetY, 1024, target.tagName === 'TEXTAREA' ? 800 : 200);
    this.fullContext.fillStyle = 'white';
    if (target.tagName === 'TEXTAREA') {
      wrapText(this.fullContext, target.value, 0, offsetY + textSize, 1024, 140);
    } else {
      this.fullContext.fillText(target.value, 0, offsetY + textSize);
    }
    this.canvasTexture.needsUpdate = true;
  }

  updateImage(event) {
    const image = event.target || event;
    this.fullContext.fillStyle = 'black';
    this.fullContext.fillRect(0, 1024, 1024, 1024);
    this.fullContext.drawImage(image, 0, 1280);
    this.canvasTexture.needsUpdate = true;
  }
}
