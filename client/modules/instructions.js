import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';

export default class Instructions {
  constructor() {
    autoBind(this);
    this.el = createEl('div', { className: 'instructions' });
    this.text1 = createEl('p', { className: 'hidden', innerText: 'Click on cans to read other TaB fan\'s reasons for saving TaB!' });
    this.text2 = createEl('p', { className: 'hidden', innerText: 'Sign your own can and everyone on Save-Tab.com will see it fall onto the pile!' });
    this.text3 = createEl('p', { className: 'hidden', innerText: 'Share the TaB love on social and help keep TaB on the shelves!' });
    addEl(this.el, this.text1, this.text2, this.text3);
    this.animations = [];
  }

  show() {
    this.animations = [];
    this.animations.push(setTimeout(() => { this.text1.classList.remove('hidden'); }, 0));
    this.animations.push(setTimeout(() => { this.text1.classList.add('hidden'); }, 3000));
    this.animations.push(setTimeout(() => { this.text2.classList.remove('hidden'); }, 4000));
    this.animations.push(setTimeout(() => { this.text2.classList.add('hidden'); }, 7000));
    this.animations.push(setTimeout(() => { this.text3.classList.remove('hidden'); }, 8000));
    this.animations.push(setTimeout(() => { this.text3.classList.add('hidden'); }, 11000));
    // this.animations.push(setTimeout(() => { this.el.classList.add('hidden'); }, 8500));
  }

  hide() {
    this.animations.forEach((animation) => {
      clearTimeout(animation);
    });

    this.text1.classList.add('hidden');
    this.text2.classList.add('hidden');
    this.text3.classList.add('hidden');
  }
}
