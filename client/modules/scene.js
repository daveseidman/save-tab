import { CircleGeometry, Vector3, PMREMGenerator, MeshStandardMaterial, MeshBasicMaterial, GridHelper, Quaternion, MeshPhysicalMaterial, Color, Object3D, Vector2, RepeatWrapping, WebGLRenderer, TextureLoader, Scene as ThreeScene, BoxGeometry, CylinderGeometry, DoubleSide, Mesh, Clock, PerspectiveCamera, CubeTextureLoader, DirectionalLight, Raycaster } from 'three';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { BokehPass } from 'three/examples/jsm/postprocessing/BokehPass';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader';
import { AmmoPhysics, PhysicsLoader } from '@enable3d/ammo-physics';
import { Tween, Easing, update as updateTweens } from '@tweenjs/tween.js';
import autoBind from 'auto-bind';
import SimplexNoise from 'simplex-noise';
import Stats from 'stats-js';
import { degToRad, lerp, debounce } from './utils';
import SoundEffects from './SoundEffects';
import Animation from './animation';

import { VignetteShader } from './VignetteShader';

import nz2 from '../assets/images/background_6.png';
import pz2 from '../assets/images/background_5.png';
import ny2 from '../assets/images/background_4.png';
import py2 from '../assets/images/background_3.png';
import nx2 from '../assets/images/background_2.png';
import px2 from '../assets/images/background_1.png';

const draco = new DRACOLoader();
draco.setDecoderPath('assets/draco/');

// TODO: share with css
const mobileWidth = 480;
const collisionThreshold = 0.005;

const spread = 10;
const radius = 1.25;
const segments = 20;
const thickness = 0.5;
const dropHeight = 5;


const maxCans = 200;

export default class Scene {
  constructor(state) {
    autoBind(this);

    this.state = state;

    this.mouse = new Vector2();
    this.mousePrev = new Vector2();
    this.mouseSpeed = { value: 0 };
    this.dragging = false;

    this.camAngle = degToRad(-20); // up / down camera angle
    this.camRotation = 0; // camera rotation
    this.rotationSpeed = 1;
    this.animationSpeed = 2500;

    this.simplex1 = new SimplexNoise('1');
    this.simplex2 = new SimplexNoise('2');

    this.physicsCans = [];
    this.farDistance = 36;
    this.nearDistance = window.innerWidth > mobileWidth ? 6 : 8;
    this.pushCanForward = 3;
    this.focusHeight = 1.5;
    this.stats = new Stats();
    this.stats.showPanel(0);
    if (window.location.hostname === 'localhost') document.body.appendChild(this.stats.dom);

    this.el = document.createElement('div');
    this.el.className = 'scene';

    this.wrapMaterial = new MeshStandardMaterial({
      transparent: true,
      alphaTest: 0.1,
      color: new Color('rgb(180, 180, 180)'),
      depthTest: false,
      roughness: 0.05,
      metalness: 0.85,
      envMapIntensity: 5,
    });

    // TODO: split into loadscene and setupscene
    this.loadPhysicsEngine().then(this.setupScene).then(() => {
      this.addTank();
      this.addFrozenCans();
      this.addLiveCans();
      this.addMarkers();
      this.resize();

      this.animation = new Animation({ can: this.canMeshHR, environment: this.environment, background: this.scene.background });

      document.body.appendChild(this.animation.el);

      // TODO: look for a way to fast forward.
      // this.physics.update(1000);
      this.render();
      this.el.dispatchEvent(new Event('loaded'));
      this.el.addEventListener('mousemove', this.mousemove);
      this.el.addEventListener('touchmove', this.mousemove);
      this.el.addEventListener('mousedown', this.mousedown);
      this.el.addEventListener('touchstart', this.mousedown);
      this.el.addEventListener('mouseup', this.mouseup);
      this.el.addEventListener('touchend', this.mouseup);
      this.el.addEventListener('mouseleave', this.mouseup);
      this.el.addEventListener('pointerleave', this.mouseup);
      this.el.addEventListener('click', this.click);
    });


    const debouncedResize = debounce(this.resize, 250);
    window.addEventListener('resize', debouncedResize);
  }

  loadPhysicsEngine() {
    return new Promise((resolve) => {
      PhysicsLoader('assets/ammo/kripken', () => resolve());
    });
  }

  addMarkers() {
    this.helpers = new Object3D();
    this.markers = [];
    const markersEl = document.createElement('div');
    markersEl.className = 'markers';
    this.el.appendChild(markersEl);
    this.scene.add(this.helpers);
    for (let i = 0; i < this.state.cans.length; i += 100) {
      const axes = new Object3D();
      axes.position.set(radius, this.totalCanHeight(i), 0);
      this.helpers.add(axes);
      const el = document.createElement('span');
      el.className = 'markers-marker';
      el.innerText = `← ${i}`;
      markersEl.appendChild(el);
      this.markers.push({ axes, el });
    }
  }

  setupScene() {
    return new Promise((resolve) => {
      this.scene = new ThreeScene();
      this.camera = new PerspectiveCamera(10, window.innerWidth / window.innerHeight, 1, 200);

      this.cameraHolder = new Object3D();
      this.scene.add(this.cameraHolder);
      this.cameraHolder.add(this.camera);

      this.renderer = new WebGLRenderer({ antialias: true });
      this.renderer.setSize(window.innerWidth, window.innerHeight);
      this.pixelRatio = 1;
      this.renderer.setPixelRatio(this.pixelRatio);
      this.composer = new EffectComposer(this.renderer);
      this.composer.addPass(new RenderPass(this.scene, this.camera));

      this.vignettePass = new ShaderPass(VignetteShader);
      this.vignettePass.uniforms.resolution.value = new Vector2(window.innerWidth * this.pixelRatio, window.innerHeight * this.pixelRatio);
      this.composer.addPass(this.vignettePass);

      this.bokehPass = new BokehPass(this.scene, this.camera, { focus: this.farDistance, aperture: 0.001, maxblur: 0.01 });
      this.composer.addPass(this.bokehPass);

      this.physics = new AmmoPhysics(this.scene, {
        gravity: { x: 0, y: -9.81, z: 0 },
        maxSubSteps: 2,
        fixedTimeStep: 1 / 90 });

      this.physics.collisionEvents.on('collision', (data) => {
        data.bodies.forEach((body) => {
          if (body.userData.id && !body.userData.collided) {
            body.userData.collided = true;
          }
        });
        if (this.soundEffects && this.soundEffects.ready
            && data.event === 'start'
            && data.bodies.length === 2
            && data.bodies[0].speed
            && data.bodies[0].speed.length() >= collisionThreshold) {
          this.soundEffects.play();
        }
      });

      this.el.appendChild(this.renderer.domElement);

      this.sun = new DirectionalLight(0xffffff, 2);
      this.sun.target.position.set(-3, -10, 2);
      // this.sun.castShadow = true;
      // this.sun.shadow.mapSize.width = 512; // default
      // this.sun.shadow.mapSize.height = 512; // default
      // this.sun.shadow.camera.near = 0.1; // default
      // this.sun.shadow.camera.far = 10; // default

      this.scene.add(this.sun);

      this.cans = new Object3D();
      this.scene.add(this.cans);

      this.clock = new Clock();

      this.raycaster = new Raycaster();

      // TODO: replace with another equiloader?
      new CubeTextureLoader().load([px2, nx2, py2, ny2, pz2, nz2], (background) => {
        // TODO: fetch all filesizes first and use to create actual load percentages
        this.el.dispatchEvent(new CustomEvent('progress', { detail: { percent: 20 } }));
        this.environment = background;
        this.wrapMaterial.envMap = this.environment;
        new TextureLoader().load('assets/images/background-blurred.png', (backgroundBlurred) => {
          this.el.dispatchEvent(new CustomEvent('progress', { detail: { percent: 40 } }));
          const pmremGenerator = new PMREMGenerator(this.renderer);
          const { texture } = pmremGenerator.fromEquirectangular(backgroundBlurred);
          this.scene.background = texture;
          new GLTFLoader().setDRACOLoader(draco).load('assets/models/tank2.gltf', (tank) => {
            this.el.dispatchEvent(new CustomEvent('progress', { detail: { percent: 60 } }));
            this.tankMesh = tank.scene.getObjectByName('Tank');
            new GLTFLoader().setDRACOLoader(draco).load('assets/models/fake-cans.gltf', (fakeCan) => {
              this.el.dispatchEvent(new CustomEvent('progress', { detail: { percent: 80 } }));

              this.fakeCans = fakeCan.scene.getObjectByName('fake-cans');
              this.fakeCans.children[0].children[0].material.envMap = this.environment;
              this.fakeCans.children[0].children[1].material.envMap = this.environment;
              this.fakeCans.children[0].children[0].material.envMapIntensity = 3;
              this.fakeCans.children[0].children[1].material.envMapIntensity = 3;
              this.scene.add(this.fakeCans);

              new GLTFLoader().setDRACOLoader(draco).load('assets/models/can6.gltf', (can) => {
                this.el.dispatchEvent(new CustomEvent('progress', { detail: { percent: 100 } }));
                this.canMeshHR = can.scene.getObjectByName('Can_HR');
                this.canMeshLR = can.scene.getObjectByName('Can_LR');
                const { min, max } = this.canMeshLR.children[0].geometry.boundingBox;
                const canRadius = (max.x - min.x) / 2;
                const canHeight = (max.y - min.y);
                // if every can could be packed with no space in between:
                const minCanVolume = Math.PI * Math.pow(canRadius, 2) * canHeight;
                // if every can took up the space of it's boundingSphere:
                const maxCanVolume = Math.pow((canHeight / 2), 3) * ((4 / 3) * Math.PI);
                // assumed volume each can will take up after simulation:
                this.avgCanVolume = lerp(minCanVolume, maxCanVolume, (6 / 8));

                this.overallHeight = this.totalCanHeight(this.state.cans.length);
                this.finalHeight = this.totalCanHeight(this.state.cans.length);
                this.camera.position.set(0, (Math.tan(this.camAngle) * -this.farDistance), this.farDistance);
                this.cameraHolder.position.y = this.totalCanHeight(this.state.cans.length - maxCans);
                this.fakeCans.position.y = this.totalCanHeight(this.state.cans.length) - 5;// -1.33; // TODO: remove this from within blender

                this.camera.rotation.x = this.camAngle;

                can.scene.traverse((obj) => {
                  if (obj.isMesh) {
                    obj.castShadow = true;
                    obj.receiveShadow = true;
                    obj.material.envMap = this.environment;
                    obj.material.envMapIntensity = 3;
                  }
                });
                this.wrap = can.scene.getObjectByName('Wrapper');

                this.wrap.material = this.wrapMaterial;
                this.wrap.renderOrder = 2;

                this.canMeshHR.add(this.wrap);
                resolve();
              });
            });
          });
        });
      });
    });
  }

  setWrapTexture(texture) {
    this.wrapMaterial.alphaMap = texture;
    this.wrapMaterial.alphaMap.wrapS = RepeatWrapping;
    this.wrapMaterial.alphaMap.wrapT = RepeatWrapping;
  }

  addTank() {
    this.tankCollider = new Mesh(new CircleGeometry(radius - thickness / 4, segments), new MeshBasicMaterial({ }));
    this.tankCollider.position.y = 0;
    this.tankCollider.rotation.x = degToRad(-90);
    this.tankCollider.visible = false;
    this.scene.add(this.tankCollider);

    this.tankWrap = new TextureLoader().load('assets/models/tank-wrap.png');
    this.tankWrap.wrapS = RepeatWrapping;
    this.tankWrap.wrapT = RepeatWrapping;
    this.tankWrap.flipY = false;
    this.tankWrap.repeat.set(1, 1 / this.finalHeight);

    this.tankVisual = this.tankMesh.clone();
    this.tankVisual.material = new MeshPhysicalMaterial({
      roughness: 0.2,
      envMap: this.environment,
      map: this.tankWrap,
      transmission: 0.95,
      side: DoubleSide,
      thickness: 0.01,
      metalness: 0.1,
      color: new Color('rgb(240, 240, 240)'),
      clearcoatMap: this.tankWrap,
      clearcoat: 1,
      ior: 1.8,
      envMapIntensity: 3,
    });

    // TODO: make this variable and maybe update every 40 cans
    this.tankVisual.scale.x = radius;
    this.tankVisual.scale.z = radius;
    this.tankVisual.scale.y = this.overallHeight + 3;
    this.tankVisual.position.y = -3;
    this.tankWrap.repeat.set(1, (this.finalHeight + 3) / 3); // TODO: this is innacurate
    this.scene.add(this.tankVisual);

    const wallMat = new MeshBasicMaterial({ transparent: true, opacity: 0.5 });
    for (let i = 0; i < segments; i += 1) {
      const wall = new Mesh(new BoxGeometry((1 * radius) * (Math.sin(180 / segments)), this.overallHeight, thickness / 4), wallMat);
      wall.position.set(Math.sin(degToRad((i / segments) * 360)) * (radius - thickness / 8), this.overallHeight / 2, Math.cos(degToRad((i / segments) * 360)) * (radius - thickness / 8));
      wall.rotation.y = degToRad((i / segments) * 360);
      this.physics.add.existing(wall, { mass: 0 });
    }
  }

  totalCanHeight(amount) {
    return (this.avgCanVolume * amount) / (Math.PI * Math.pow(radius, 2));
  }

  addFrozenCans() {
    this.fakeCanCollider = new Mesh(new CylinderGeometry(radius, radius, 2, 24, 2), new MeshBasicMaterial({ color: 0xffffff, wireframe: true }));
    this.fakeCanCollider.position.y = this.totalCanHeight(this.state.cans.length - maxCans) - 1;
    this.physics.add.existing(this.fakeCanCollider, { mass: 0 });
  }

  addLiveCans() {
    this.fakeCanAmount = this.state.cans.length - maxCans;
    for (let i = 0; i < this.state.cans.length; i += 1) {
      if (i >= this.fakeCanAmount) {
        // for (let i = 0; i < maxCans; i += 1) {
        setTimeout(() => {
          if (i === this.state.cans.length - 1) this.state.addingCans = false;
          const can = this.addCan(i);
          this.cans.add(can);
          this.physicsCans[i] = can;
          this.overallHeight = this.totalCanHeight(this.physicsCans.length);
        }, (i - this.fakeCanAmount) * 60); // TODO: set this based on how long we want to be adding cans
      } else {
        this.physicsCans[i] = null;
      }
    }
  }

  addCan(i) {
    // TODO: pull radius, width, height, values from model
    const can = this.physics.factory.add.cylinder({ radiusBottom: 0.1, radiusTop: 0.1, height: 0.4 }, { lambert: { visible: false } });
    const canMesh = this.canMeshLR.clone();
    // const spread = 10; // TODO: multiplier of radius?
    const x = ((Math.random() * radius) * spread) - (spread / 2);// - (radius / 2);
    const z = ((Math.random() * radius) * spread) - (spread / 2);// - (radius / 2);
    const y = this.finalHeight + dropHeight;
    can.position.set(x, y, z);
    can.positionPrev = can.position.clone();
    can.rotation.x = Math.random() * Math.PI;
    can.rotation.z = Math.random() * Math.PI;
    can.userData.id = i || this.physicsCans.length;
    can.userData.collided = false;
    can.add(canMesh);
    this.physics.add.existing(can, { mass: 1 });
    // TODO: play with these values:
    // can.body.setRestitution(0.75);
    // can.body.setFriction(0.5);
    // can.body.setDamping(0.1, 0.1);
    // can.body.setBounciness(0.5);
    // can.body.setCcdSweptSphereRadius(1);
    can.body.applyForce(-x, 0, -z);
    can.body.applyTorque(Math.random() * 10, Math.random() * 10, Math.random() * 10);
    can.body.checkCollisions = true;
    return can;
  }

  createAnimation() {
    this.animation.generate().then((animation) => { this.state.animation = animation; });
  }

  mousemove(e) {
    this.mouseMoved = true;
    const clientX = e.changedTouches ? e.changedTouches[0].clientX : e.clientX;
    const clientY = e.changedTouches ? e.changedTouches[0].clientY : e.clientY;
    this.mouse.x = (clientX / window.innerWidth) * 2 - 1;
    this.mouse.y = -(clientY / window.innerHeight) * 2 + 1;

    if (this.dragging) {
      this.camRotation += this.mouseSpeed.value;
    } else {
      this.raycaster.setFromCamera(this.mouse, this.camera);
      const intersects = this.raycaster.intersectObject(this.tankCollider);
      if (intersects.length) {
        this.point = intersects[0].point;
        const velocity2D = this.mouse.clone().sub(this.mousePrev).multiplyScalar(5);
        const velocity3D = new Vector3(velocity2D.x, 0, -velocity2D.y).applyAxisAngle(new Vector3(0, 1, 0), this.camRotation);
        // TODO: make this loop tighter, start at totalcans - maxcans, loop maxcans times
        this.physicsCans.forEach((can) => {
          if (can) {
            const distance = can.position.distanceTo(this.point);
            if (distance < 1) {
              can.body.applyForce(
                velocity3D.x * (1 / distance),
                velocity2D.y * (1 / distance),
                velocity3D.z * (1 / distance),
              );
            }
          }
        });
      }
    }
    // TODO: this is jumpy between mouseup and mousedown, maybe skip calculation if mousedown happened this frame
    this.mouseSpeed = { value: this.mouse.clone().sub(this.mousePrev).x * (this.mouse.y > 0 ? 1 : -1) }; // TODO: this.mouse.y > Y POS OF TANK COLLIDER
    this.mousePrev = this.mouse.clone();
  }

  mousedown(e) {
    if (this.state.can !== null) return;
    this.mouseMoved = false;
    const clientX = e.changedTouches ? e.changedTouches[0].clientX : e.clientX;
    const clientY = e.changedTouches ? e.changedTouches[0].clientY : e.clientY;
    this.mouse.x = (clientX / window.innerWidth) * 2 - 1;
    this.mouse.y = -(clientY / window.innerHeight) * 2 + 1;
    this.raycaster.setFromCamera(this.mouse, this.camera);
    const intersects = this.raycaster.intersectObject(this.tankCollider);
    this.dragging = intersects.length === 0;
  }

  mouseup() {
    if (this.dragging) {
      if (Math.abs(this.mouseSpeed.value) > 0) {
        this.dragDampenTween = new Tween(this.mouseSpeed)
          .to({ value: 0 }, 1000)
          .onUpdate(({ value }) => {
            this.camRotation += value;
          })
          .start();
      }
    }
    this.dragging = false;
  }

  click() {
    if (this.physicsCans.length + this.fakeCanAmount < this.state.cans.length) return; // cans still pouring in
    if (this.canTween || this.camTween) return;
    if (this.state.view) return;
    if (this.mouseMoved) return;
    const intersects = this.raycaster.intersectObjects(this.cans.children, false);
    if (intersects.length) {
      this.state.can = intersects[0].object.userData.id;
    }
  }


  pickupCan(id) {
    let can;
    if (this.physicsCans[id]) {
      can = this.physicsCans[id];
    } else {
      can = this.addCan();
      this.physicsCans.push(can);
      // TODO: put can in far background and tween it forward
    }
    // const can = this.physicsCans[id] ? this.physicsCans[id] : this.addCan();

    if (this.canTween) {
      // TODO: this is not enough, still can get to a broken state
      this.canTween.stop();
    }

    can.remove(can.getObjectByName('Can_LR'));
    can.add(this.canMeshHR.clone());

    const canPositionStart = can.position.clone();
    // const canPositionEnd = new Vector3(0, this.overallHeight + this.focusHeight, 0);
    // current rotation plus rotation that will take place in next (animationSpeed) msecs
    const angle = this.camRotation;// + 0.07165; // + (this.animationSpeed * this.rotationSpeed);
    const canPositionEnd = new Vector3(Math.sin(angle) * this.pushCanForward, this.overallHeight + this.focusHeight, Math.cos(angle) * this.pushCanForward);

    const canRotationStart = can.quaternion.clone();
    const canRotationEnd = new Quaternion().setFromAxisAngle(new Vector3(0, 1, 0), this.camRotation + degToRad(180) - degToRad(30));
    const canRotationEnd2 = new Quaternion().setFromAxisAngle(new Vector3(0, 1, 0), this.camRotation + degToRad(180) + degToRad(30));
    can.body.setCollisionFlags(2);

    const cameraPositionStart = this.camera.position.clone();
    const cameraPositionEnd = new Vector3(0, this.focusHeight + (Math.tan(this.camAngle) * (-this.nearDistance + this.pushCanForward)), this.nearDistance);
    this.rotationSpeed = 0; // TODO <- tween this

    this.canTween = new Tween({ percent: 0 }).to({ percent: 1 }, this.animationSpeed)
      .easing(Easing.Quintic.InOut)
      .onUpdate(({ percent }) => {
        can.position.lerpVectors(canPositionStart, canPositionEnd, percent);
        can.quaternion.slerpQuaternions(canRotationStart, canRotationEnd, percent);
        can.body.needUpdate = true;
        this.camera.position.lerpVectors(cameraPositionStart, cameraPositionEnd, percent);
        this.bokehPass.uniforms.aperture.value = lerp(0.001, 0.01, percent);
        this.bokehPass.uniforms.focus.value = lerp(this.farDistance, (this.nearDistance - this.pushCanForward), percent);
      })
      .start()
      .onComplete(() => {
        this.canTween = null;
        this.oscillateCanTween = new Tween({ percent: 0 }).to({ percent: 1 }, 3000)
          .easing(Easing.Quadratic.InOut)
          .repeat(Infinity)
          .yoyo(true)
          .onUpdate(({ percent }) => {
            can.quaternion.slerpQuaternions(canRotationEnd, canRotationEnd2, percent);
            if (can.body) can.body.needUpdate = true;
          })
          .start();
      });
  }

  dropCan(id) {
    const can = this.physicsCans[id];
    can.remove(can.getObjectByName('Can_HR'));
    can.add(this.canMeshLR.clone());
    can.body.setCollisionFlags(0);
    this.physics.add.existing(can, { mass: 1 });
    can.body.applyForce(can.position.x * -2, 0, can.position.z * -2);
    can.body.applyTorque(Math.random() * 5, Math.random() * 5, Math.random() * 5);

    const cameraPositionStart = this.camera.position.clone();
    const cameraPositionEnd = new Vector3(0, (Math.tan(this.camAngle) * -this.farDistance), this.farDistance);

    if (this.oscillateCanTween) {
      this.oscillateCanTween.stop();
      this.oscillateCanTween = null;
    }

    this.rotationSpeed = 1;
    this.canTween = new Tween({ percent: 0 }).to({ percent: 1 }, this.animationSpeed)
      .easing(Easing.Quintic.InOut)
      .onUpdate(({ percent }) => {
        this.rotationSpeed = percent;
        this.camera.position.lerpVectors(cameraPositionStart, cameraPositionEnd, percent);
        this.bokehPass.uniforms.focus.value = lerp(this.farDistance, (this.nearDistance - this.pushCanForward), (1 - percent));
        this.bokehPass.uniforms.aperture.value = lerp(0.001, 0.01, (1 - percent));
      })
      .onComplete(() => {
        this.canTween = null;
      })
      .start();
  }

  centerCamera(centered, immediate) {
    const ww = window.innerWidth;
    const wh = window.innerHeight;
    const offsetX = centered ? 0 : window.innerWidth >= mobileWidth ? ww / -6 : 0;
    const offsetY = centered ? 0 : window.innerWidth < mobileWidth ? wh / -6 : 0;
    const offset = {
      x: this.camera.view.offsetX,
      y: this.camera.view.offsetY,
    };
    this.camTween = new Tween(offset).to({ x: offsetX, y: offsetY }, immediate ? 0 : this.animationSpeed)
      .easing(Easing.Quintic.InOut)
      .onUpdate(({ x, y }) => {
        this.camera.setViewOffset(ww, wh, x, y, ww, wh);
      })
      .onComplete(() => {
        this.camTween = null;
      })
      .start();
  }

  mute() {
    this.soundEffects.volumeNode.gain.value = 0;
  }

  unmute() {
    if (this.soundEffects) {
      this.soundEffects.volumeNode.gain.value = 1;
    } else {
      this.soundEffects = new SoundEffects();
      this.soundEffects.load();// .then(() => {
      this.soundEffects.volumeNode.gain.value = 1;
      // });
    }
  }

  render() {
    this.stats.begin();

    updateTweens();
    this.physics.update(this.clock.getDelta() * 1000);

    // TODO: this can be a tigher loop (start at fakecanamount, loop maxcan times)
    this.physicsCans.forEach((can, index) => {
      if (can) {
        can.speed = can.position.clone().sub(can.positionPrev);
        can.positionPrev = can.position.clone();
        if (can.position.y < -5) { // use set distance from overall height instead of -5
          this.physics.destroy(can);
          this.cans.remove(can);
          can = this.addCan(index);
          this.physicsCans[index] = can;
          this.cans.add(can);
        }
      }
    });

    this.camRotation += (this.clock.getDelta() / 4) * this.rotationSpeed;

    // TODO: maybe replace these with tween since it only happens the first few seconds
    this.cameraHolder.rotation.y = this.camRotation;
    this.cameraHolder.position.y += (this.overallHeight - this.cameraHolder.position.y) / 10;
    this.tankCollider.position.y += (this.overallHeight - this.tankCollider.position.y) / 10;

    const ww = window.innerWidth;
    const wh = window.innerHeight;
    this.helpers.rotation.y = this.camRotation;
    this.helpers.updateMatrix();
    this.markers.forEach(({ axes, el }) => {
      axes.updateWorldMatrix();
      const vec = new Vector3();
      axes.localToWorld(vec);
      vec.project(this.camera);
      vec.x = vec.x * (ww / 2) + (ww / 2);
      vec.y = -(vec.y * (wh / 2)) + wh / 2;
      el.style.left = `${vec.x}px`;
      el.style.top = `${vec.y}px`;
    });

    this.noise1 = this.simplex1.noise2D(1, this.clock.getElapsedTime() / 5);
    this.noise2 = this.simplex2.noise2D(1, this.clock.getElapsedTime() / 5);
    // TODO: make sure these get reset
    // maybe uneccesary since we remove and add low / high res meshes
    if (this.state.can) {
      this.physicsCans[this.state.can].children[0].rotation.x = this.noise1 / 6;
      this.physicsCans[this.state.can].children[0].rotation.z = this.noise2 / 6;
    }

    this.composer.render();

    this.stats.end();

    requestAnimationFrame(this.render);
  }

  resize() {
    const ww = window.innerWidth;
    const wh = window.innerHeight;
    this.camera.aspect = ww / wh;
    this.camera.updateProjectionMatrix();
    this.nearDistance = window.innerWidth > mobileWidth ? 6 : 8;

    this.renderer.setSize(ww, wh);
    this.composer.setSize(ww, wh);

    const offsetX = (this.state.can !== null || this.state.view === 'info' || this.state.view === 'share') ? (ww > mobileWidth ? (-ww / 6) : 0) : 0;
    const offsetY = (this.state.can !== null || this.state.view === 'info' || this.state.view === 'share') ? (ww < mobileWidth ? (-wh / 6) : 0) : 0;
    this.camera.setViewOffset(ww, wh, offsetX, offsetY, ww, wh);

    this.vignettePass.uniforms.resolution.value = new Vector2(ww * this.pixelRatio, wh * this.pixelRatio);
  }
}
