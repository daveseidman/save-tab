import { createEl, addEl } from 'lmnt';

export default class Menu {
  constructor(state) {
    this.state = state;
    this.el = createEl('div', { className: 'menu hidden' });
    this.signButton = createEl('button', { className: 'menu-sign', innerText: 'Sign Your Can' }, {}, { click: () => { if (this.state.addingCans) return; this.state.can = this.state.cans.length; } });
    this.viewButton = createEl('button', { className: 'menu-view hidden', innerText: 'View Your Can' }, {}, { click: () => { this.state.can = this.state.userID; } });
    const infoButton = createEl('button', { className: 'menu-info secondary', innerText: 'More Info' }, {}, { click: () => { this.state.view = 'info'; } });

    this.shareButton = createEl('button', { className: 'menu-share secondary', innerText: 'Share the ♥' }, {}, { click: () => { this.state.view = 'share'; } });

    addEl(this.el, this.signButton, this.viewButton, infoButton, this.shareButton);
  }

  show() {
    this.el.classList.remove('hidden');
  }

  hide() {
    this.el.classList.add('hidden');
  }
}
