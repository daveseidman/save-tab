import { createEl, addEl } from 'lmnt';

export default class Footer {
  constructor(state) {
    this.state = state;

    this.el = createEl('div', { className: 'footer' });
    this.muteBtn = createEl('btn', { className: 'footer-mute muted' }, {}, { click: () => { this.state.mute = !this.state.mute; } });
    this.credit = createEl('p', { className: 'footer-credit', innerHTML: 'A Digital Stunt by <a target="_blank" href="https://daveseidman.com">Dave Seidman</a>' });

    // this.twitter = createEl('a', { href: 'https://twitter.com/DaveSeidman?ref_src=twsrc%5Etfw', className: 'twitter-follow-button', innerText: 'Follow @DaveSeidman' }, { 'data-show-count': false });

    addEl(this.el, this.muteBtn, this.credit);// , this.twitter);
  }

  mute() {
    this.muteBtn.classList.add('muted');
  }

  unmute() {
    this.muteBtn.classList.remove('muted');
  }
}
