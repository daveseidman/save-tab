import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';

export default class ShareView {
  constructor(state) {
    autoBind(this);
    this.state = state;
    this.el = createEl('div', { className: 'views-share hidden' });

    this.main = createEl('div', { className: 'main' });
    this.wrap = createEl('div', { className: 'wrap' });
    this.title = createEl('h1', { className: 'views-share-title', innerText: 'Share this TaB!' });
    this.platforms = createEl('div', { className: 'views-share-platforms' });
    this.shareTwitter = createEl('a', { className: 'views-share-platforms-twitter', innerText: 'Share on Twitter', href: 'https://twitter.com/intent/tweet?url=save-tab.com%2F&text=Save+TaB!&hashtags=coke,tabsoda', target: '_blank' });
    this.shareFacebook = createEl('a', { className: 'views-share-platforms-facebook', innerText: 'Share on Facebook', href: 'https://www.facebook.com/sharer.php?u=http%3A%2F%2Fsave-tab.com%2F', target: '_blank' });
    this.shareLinkedIn = createEl('a', { className: 'views-share-platforms-linkedin', innerText: 'Share on LinkedIn', href: 'https://www.linkedin.com/sharing/share-offsite/?url=save-tab.com', target: '_blank' });

    this.animationContainer = createEl('div', { className: 'views-share-animation' });

    this.downloadBtn = createEl('a', { className: 'views-share-download hidden', innerText: 'Download Your Can!' }, { download: 'Save_TaB.gif' }, {});
    const returnBtn = createEl('button', { className: 'secondary', innerText: 'Return to TaB' }, {}, { click: () => { this.state.view = null; } });

    addEl(this.platforms, this.shareTwitter, this.shareFacebook, this.shareLinkedIn);
    addEl(this.main, this.wrap);
    addEl(this.wrap, this.title, this.platforms, this.animationContainer, this.downloadBtn, returnBtn);
    addEl(this.el, this.main);
  }

  show() {
    this.el.classList.remove('hidden');
  }

  hide() {
    this.el.classList.add('hidden');
  }

  updateAnimation(animation) {
    this.animationContainer.innerHTML = '';
    addEl(this.animationContainer, animation);
    this.downloadBtn.href = animation.src;
    this.downloadBtn.classList.remove('hidden');
  }
}
