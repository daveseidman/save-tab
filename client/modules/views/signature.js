import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';
import { load as loadRecaptcha } from 'recaptcha-v3';
import SignaturePad from '../signature_pad';
import { recaptchaToken } from '../utils';

export default class SignatureView {
  constructor(state, canLabel) {
    autoBind(this);
    this.state = state;
    this.canLabel = canLabel;

    // this.valid = false // TODO: implement this for elegance

    this.el = createEl('div', { className: 'views-signature hidden' });

    this.form = createEl('form', { className: 'main' });
    const nameLabel = createEl('label', { innerText: 'Your Name:' });
    this.name = createEl('input', { type: 'text', placeholder: 'First, Last Initial', minLength: 2, maxLength: 50 }, { offsetY: 0, required: true }, { input: this.inputUpdated });

    const row1 = createEl('div', { className: 'row' });
    addEl(row1, nameLabel, this.name);
    const locationLabel = createEl('label', { innerText: 'Your Location:' });
    this.location = createEl('input', { type: 'text', placeholder: 'City, State', minLength: 2, maxLength: 50 }, { offsetY: 200, required: true }, { input: this.inputUpdated });

    const row2 = createEl('div', { className: 'row' });
    addEl(row2, locationLabel, this.location);

    const column1 = createEl('div', { className: 'column first' });
    addEl(column1, row1, row2);
    const reasonLabel = createEl('label', { innerText: 'Your reason for saving TaB:' });
    this.reason = createEl('textarea', { placeholder: 'I think Coca Cola should save TaB because...', minLength: 2, maxlength: 100 }, { offsetY: 400, required: true }, { input: this.inputUpdated });

    this.forms = [this.name, this.location, this.reason];

    const label = createEl('label', { innerText: 'Sign your can below:' });
    const container = createEl('div', { className: 'pad' });
    this.canvas = createEl('canvas', { className: 'pad-canvas', width: 1024, height: 512 });
    this.pad = new SignaturePad(this.canvas, { minWidth: 5, maxWidth: 15, penColor: 'rgb(255, 255, 255)', backgroundColor: 'rgb(0, 0, 0)' });
    this.canvas.addEventListener('update', () => {
      this.canLabel.updateImage(this.canvas);
      this.signatureStarted = true;
      this.inputUpdated();
      document.activeElement.blur();
    });
    this.signature = createEl('img', { draggable: false });
    this.signature.addEventListener('load', this.canLabel.updateImage);
    this.resetBtn = createEl('button', { className: 'pad-reset' }, {}, {
      click: (e) => {
        e.preventDefault();
        this.pad.clear();
        this.canLabel.updateImage(this.canvas);
        this.signatureStarted = false;
        this.inputUpdated();
      } });
    this.disclaimer = createEl('label', { className: 'disclaimer', innerText: 'please don\'t enter any personal information such are your full address or phone number' });
    this.submitBtn = createEl('button', { className: 'disabled', innerText: 'Submit' }, {}, { click: this.submit });
    const returnBtn = createEl('button', { className: 'return secondary', innerText: 'Return to TaB' }, {}, { click: (e) => { e.preventDefault(); this.state.can = null; } });
    const column2 = createEl('div', { className: 'column' });
    addEl(column2, this.submitBtn, returnBtn);
    addEl(container, this.canvas, this.signature, this.resetBtn);
    addEl(this.form, column1, reasonLabel, this.reason, label, container, column2, this.disclaimer);
    addEl(this.el, this.form);

    loadRecaptcha(recaptchaToken).then((recaptcha) => {
      this.recaptcha = recaptcha;
    });
  }

  show() {
    if (this.state.cans[this.state.can]) {
      const { name, location, reason, signature } = this.state.cans[this.state.can];
      this.name.value = name;
      this.location.value = location;
      this.reason.value = reason;
      this.el.classList.add('readonly');
      this.canLabel.updateText(this.name);
      this.canLabel.updateText(this.location);
      this.canLabel.updateText(this.reason);
      this.signature.src = `/api/signature/${signature}`;
    } else {
      this.signatureStarted = false;
      this.pad.clear();
      this.form.reset();
      this.canLabel.clear();
      this.canLabel.updateImage(this.canvas);
      this.el.classList.remove('readonly');
    }

    this.el.classList.remove('hidden');
  }

  hide() {
    this.el.classList.add('hidden');
  }

  inputUpdated(e) {
    if (e) this.canLabel.updateText(e);
    this.submitBtn.classList[this.form.checkValidity() && this.signatureStarted ? 'remove' : 'add']('disabled');
  }

  submit(e) {
    e.preventDefault();
    // TODO: make a checkValidity method that sets a this.valid boolean and use that instead

    if (this.submitBtn.classList.contains('disabled')) {
      this.form.classList.add('show-invalid');
      setTimeout(() => {
        this.form.classList.remove('show-invalid');
      }, 2500);

      if (!this.signatureStarted) {
        this.canvas.classList.add('show-invalid');
        setTimeout(() => {
          this.canvas.classList.remove('show-invalid');
        }, 2500);
      }

      return;
    }


    if (!this.form.checkValidity()) return;

    this.recaptcha.execute('submit').then((token) => {
      this.form.classList.add('disabled');
      const data = {
        name: this.name.value,
        location: this.location.value,
        reason: this.reason.value,
        signature: this.pad.toDataURL(),
        socket: this.state.socket,
        token,
      };
      fetch('/api/signature', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data),
      }).then(res => res.json()).then((res) => {
        data.signature = res.filename;
        this.state.cans.push(data);
        this.form.classList.remove('disabled');
        this.state.canSigned = true;
      });
    });
  }
}
