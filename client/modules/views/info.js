import { createEl, addEl } from 'lmnt';

const titles = [
  'Yes we <span class="can">Can!</span>',
  'A <span class="can">Can</span> on a Mission!',
  'Join the <span class="can">Can</span>paign!',
];

export default class InfoView {
  constructor(state) {
    this.state = state;
    this.el = createEl('div', { className: 'views-start hidden' });
    this.main = createEl('div', { className: 'main' });
    this.wrap = createEl('div', { className: 'wrap' });
    this.title = createEl('h1', { className: 'views-start-title', innerHTML: titles[Math.floor(Math.random() * titles.length)] });
    const text1 = createEl('p', { className: 'views-start-text', innerText: 'On October 16, 2020 the Coca-Cola Company announced they would discontinue TaB–the original diet cola–after 57 years of production.' });
    const text2 = createEl('p', { className: 'views-start-text', innerHTML: 'Please visit <a href="http://savetabsoda.com" target="_blank">SaveTabSoda.com</a> and sign the petition to keep TaB on the shelves.' });
    this.startButton = createEl('button', { className: 'views-start-button', innerText: 'Get Started!' }, {}, { click: () => { this.state.view = null; if (!this.state.cansDropped) this.state.cansDropped = true; } });
    addEl(this.main, this.wrap);
    addEl(this.wrap, this.title, text1, text2, this.startButton);
    addEl(this.el, this.main);
  }

  show() {
    this.title.innerHTML = titles[Math.floor(Math.random() * titles.length)];
    this.el.classList.remove('hidden');
  }

  hide() {
    this.el.classList.add('hidden');
  }

  enable() {
    this.startButton.classList.remove('disabled');
  }
}
