import autoBind from 'auto-bind';
import { createEl, addEl } from 'lmnt';
import { Scene, WebGLRenderer, PerspectiveCamera, DirectionalLight } from 'three';
import { createGIF } from 'gifshot';
import { degToRad } from './utils';

// console.log(gifshot);

const degrees = 360;
const frames = 20;
export default class Animation {
  constructor(objects) {
    autoBind(this);
    this.can = objects.can;

    this.el = createEl('div', { className: 'test' });
    this.scene = new Scene();
    this.scene.background = objects.background;
    this.renderer = new WebGLRenderer({ antialias: true });
    this.renderer.setSize(256, 256);
    this.camera = new PerspectiveCamera(10, 1, 1, 10);
    this.camera.position.z = 1.5;
    this.camera.rotation.z = degToRad(30);
    this.scene.add(this.can);
    const light = new DirectionalLight(0xffffff, 2);
    light.target.position.set(-3, -10, 2);
    this.scene.add(light);
  }

  generate() {
    return new Promise((resolve) => {
      const images = [];
      for (let i = 0; i < degrees; i += (degrees / frames)) {
        this.can.rotation.y = degToRad(i);
        this.renderer.render(this.scene, this.camera);
        images.push(this.renderer.domElement.toDataURL());
      }
      createGIF({
        images,
      }, (obj) => {
        if (!obj.error) {
          const image = obj.image;
          const animation = createEl('img', { src: image });
          resolve(animation);
        }
      });
    });
  }
}
