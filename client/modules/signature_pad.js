
class Point {
  constructor(x, y, time) {
    this.x = x;
    this.y = y;
    this.time = time || new Date().getTime();
  }

  velocityFrom(start) {
    return (this.time !== start.time) ? this.distanceTo(start) / (this.time - start.time) : 1;
  }

  distanceTo(start) {
    return Math.sqrt(Math.pow(this.x - start.x, 2) + Math.pow(this.y - start.y, 2));
  }
}


class Bezier {
  constructor(startPoint, control1, control2, endPoint) {
    this.startPoint = startPoint;
    this.control1 = control1;
    this.control2 = control2;
    this.endPoint = endPoint;
  }

  length() {
    const steps = 10;
    let length = 0;
    let i; let t; let cx; let cy; let px; let py; let xdiff; let
      ydiff;

    for (i = 0; i <= steps; i += 1) {
      t = i / steps;
      cx = this._point(t, this.startPoint.x, this.control1.x, this.control2.x, this.endPoint.x);
      cy = this._point(t, this.startPoint.y, this.control1.y, this.control2.y, this.endPoint.y);
      if (i > 0) {
        xdiff = cx - px;
        ydiff = cy - py;
        length += Math.sqrt(xdiff * xdiff + ydiff * ydiff);
      }
      px = cx;
      py = cy;
    }
    return length;
  }

  _point(t, start, c1, c2, end) {
    return start * (1.0 - t) * (1.0 - t) * (1.0 - t)
                 + 3.0 * c1 * (1.0 - t) * (1.0 - t) * t
                 + 3.0 * c2 * (1.0 - t) * t * t
                 + end * t * t * t;
  }
}


export default class SignaturePad {
  constructor(canvas, options) {
    const self = this;
    const opts = options || {};

    this.velocityFilterWeight = opts.velocityFilterWeight || 0.7;
    this.minWidth = opts.minWidth || 0.5;
    this.maxWidth = opts.maxWidth || 2.5;
    this.dotSize = opts.dotSize || function () {
      return (this.minWidth + this.maxWidth) / 2;
    };
    this.penColor = opts.penColor || 'black';
    this.backgroundColor = opts.backgroundColor || 'rgba(0,0,0,0)';
    this.onEnd = opts.onEnd;
    this.onBegin = opts.onBegin;

    this._canvas = canvas;
    this._ctx = canvas.getContext('2d');
    this.clear();

    this._handleMouseEvents();
    this._handleTouchEvents();
  }

  clear() {
    const ctx = this._ctx;
    const canvas = this._canvas;

    ctx.fillStyle = this.backgroundColor;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    this._reset();
  }

  toDataURL(imageType, quality) {
    const canvas = this._canvas;
    return canvas.toDataURL.apply(canvas, arguments);
  }

  fromDataURL(dataUrl) {
    const self = this;
    const image = new Image();
    const ratio = window.devicePixelRatio || 1;
    const width = this._canvas.width / ratio;
    const height = this._canvas.height / ratio;

    this._reset();
    image.src = dataUrl;
    image.onload = function () {
      self._ctx.drawImage(image, 0, 0, width, height);
    };
    this._isEmpty = false;
  }

  _strokeUpdate(event) {
    const point = this._createPoint(event);
    this._addPoint(point);
    this._canvas.dispatchEvent(new Event('update'));
  }

  _strokeBegin(event) {
    this._reset();
    this._strokeUpdate(event);
    if (typeof this.onBegin === 'function') {
      this.onBegin(event);
    }
  }

  _strokeDraw(point) {
    const ctx = this._ctx;
    const dotSize = typeof (this.dotSize) === 'function' ? this.dotSize() : this.dotSize;

    ctx.beginPath();
    this._drawPoint(point.x, point.y, dotSize);
    ctx.closePath();
    ctx.fill();
  }

  _strokeEnd(event) {
    const canDrawCurve = this.points.length > 2;
    const point = this.points[0];

    if (!canDrawCurve && point) {
      this._strokeDraw(point);
    }
    if (typeof this.onEnd === 'function') {
      this.onEnd(event);
    }
    this._canvas.dispatchEvent(new Event('end'));
  }

  _handleMouseEvents() {
    const self = this;
    this._mouseButtonDown = false;

    this._canvas.addEventListener('mousedown', (event) => {
      if (event.which === 1) {
        self._mouseButtonDown = true;
        self._strokeBegin(event);
      }
    });

    this._canvas.addEventListener('mousemove', (event) => {
      if (self._mouseButtonDown) {
        self._strokeUpdate(event);
      }
    });

    document.addEventListener('mouseup', (event) => {
      if (event.which === 1 && self._mouseButtonDown) {
        self._mouseButtonDown = false;
        self._strokeEnd(event);
      }
    });
  }

  _handleTouchEvents() {
    const self = this;

    // Pass touch events to canvas element on mobile IE.
    this._canvas.style.msTouchAction = 'none';

    this._canvas.addEventListener('touchstart', (event) => {
      const touch = event.changedTouches[0];
      self._strokeBegin(touch);
    });

    this._canvas.addEventListener('touchmove', (event) => {
    // Prevent scrolling.
      event.preventDefault();

      const touch = event.changedTouches[0];
      self._strokeUpdate(touch);
    });

    document.addEventListener('touchend', (event) => {
      const wasCanvasTouched = event.target === self._canvas;
      if (wasCanvasTouched) {
        self._strokeEnd(event);
      }
    });
  }

  isEmpty() {
    return this._isEmpty;
  }

  _reset() {
    this.points = [];
    this._lastVelocity = 0;
    this._lastWidth = (this.minWidth + this.maxWidth) / 2;
    this._isEmpty = true;
    this._ctx.fillStyle = this.penColor;
  }

  _createPoint(event) {
    const rect = this._canvas.getBoundingClientRect();
    const scaleX = this._canvas.width / rect.width;
    const scaleY = this._canvas.height / rect.height;

    return new Point(
      (event.clientX - rect.left) * scaleX,
      (event.clientY - rect.top) * scaleY,
    );
  }

  _addPoint(point) {
    const points = this.points;
    let c2; let c3;
    let curve; let
      tmp;

    points.push(point);

    if (points.length > 2) {
    // To reduce the initial lag make it work with 3 points
    // by copying the first point to the beginning.
      if (points.length === 3) points.unshift(points[0]);

      tmp = this._calculateCurveControlPoints(points[0], points[1], points[2]);
      c2 = tmp.c2;
      tmp = this._calculateCurveControlPoints(points[1], points[2], points[3]);
      c3 = tmp.c1;
      curve = new Bezier(points[1], c2, c3, points[2]);
      this._addCurve(curve);

      // Remove the first element from the list,
      // so that we always have no more than 4 points in points array.
      points.shift();
    }
  }

  _calculateCurveControlPoints(s1, s2, s3) {
    const dx1 = s1.x - s2.x; const dy1 = s1.y - s2.y;
    const dx2 = s2.x - s3.x; const dy2 = s2.y - s3.y;

    const m1 = { x: (s1.x + s2.x) / 2.0, y: (s1.y + s2.y) / 2.0 };
    const m2 = { x: (s2.x + s3.x) / 2.0, y: (s2.y + s3.y) / 2.0 };

    const l1 = Math.sqrt(dx1 * dx1 + dy1 * dy1);
    const l2 = Math.sqrt(dx2 * dx2 + dy2 * dy2);

    const dxm = (m1.x - m2.x);
    const dym = (m1.y - m2.y);

    const k = l2 / (l1 + l2);
    const cm = { x: m2.x + dxm * k, y: m2.y + dym * k };

    const tx = s2.x - cm.x;
    const ty = s2.y - cm.y;

    return {
      c1: new Point(m1.x + tx, m1.y + ty),
      c2: new Point(m2.x + tx, m2.y + ty),
    };
  }

  _addCurve(curve) {
    const startPoint = curve.startPoint;
    const endPoint = curve.endPoint;
    let velocity; let
      newWidth;

    velocity = endPoint.velocityFrom(startPoint);
    velocity = this.velocityFilterWeight * velocity
            + (1 - this.velocityFilterWeight) * this._lastVelocity;

    newWidth = this._strokeWidth(velocity);
    this._drawCurve(curve, this._lastWidth, newWidth);

    this._lastVelocity = velocity;
    this._lastWidth = newWidth;
  }

  _drawPoint(x, y, size) {
    const ctx = this._ctx;

    ctx.moveTo(x, y);
    ctx.arc(x, y, size, 0, 2 * Math.PI, false);
    this._isEmpty = false;
  }

  _drawCurve(curve, startWidth, endWidth) {
    const ctx = this._ctx;
    const widthDelta = endWidth - startWidth;
    let drawSteps; let width; let i; let t; let tt; let ttt; let u; let uu; let uuu; let x; let
      y;

    drawSteps = Math.floor(curve.length());
    ctx.beginPath();
    for (i = 0; i < drawSteps; i++) {
    // Calculate the Bezier (x, y) coordinate for this step.
      t = i / drawSteps;
      tt = t * t;
      ttt = tt * t;
      u = 1 - t;
      uu = u * u;
      uuu = uu * u;

      x = uuu * curve.startPoint.x;
      x += 3 * uu * t * curve.control1.x;
      x += 3 * u * tt * curve.control2.x;
      x += ttt * curve.endPoint.x;

      y = uuu * curve.startPoint.y;
      y += 3 * uu * t * curve.control1.y;
      y += 3 * u * tt * curve.control2.y;
      y += ttt * curve.endPoint.y;

      width = startWidth + ttt * widthDelta;
      this._drawPoint(x, y, width);
    }
    ctx.closePath();
    ctx.fill();
  }

  _strokeWidth(velocity) {
    return Math.max(this.maxWidth / (velocity + 1), this.minWidth);
  }
}
