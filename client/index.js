import autoBind from 'auto-bind';
import onChange from 'on-change';
import { createEl, addEl } from 'lmnt';
import io from 'socket.io-client';
import SignatureView from './modules/views/signature';
import InfoView from './modules/views/info';
import ShareView from './modules/views/share';
import Footer from './modules/views/footer';
import Menu from './modules/views/menu';
import Scene from './modules/scene';
import Label from './modules/label';
import Instructions from './modules/instructions';
import { shuffle } from './modules/utils';
import './index.scss';

class App {
  constructor() {
    autoBind(this);

    const state = {
      view: null, // [info, signature, share]
      can: null, // when not null, show details for this can
      animation: null,
      userID: null, // latest can id this user signed
      cans: [],
      mute: true,
      socket: null,
      canSigned: false,
      firstLoad: true,
      firstGesture: false,
      addingCans: true,
    };

    this.state = onChange(state, this.update, { ignoreKeys: ['cans'] });

    this.socket = io('/savetab');

    this.socket.on('connect', () => { this.state.socket = this.socket.id; });
    this.socket.on('canAdded', (data) => {
      if (data.socket !== this.state.socket) {
        delete data.socket;
        this.state.cans.push(data);
        // TODO: this should be in scene.js
        this.scene.physicsCans.push(this.scene.addCan(this.state.cans.length));
      }
      // this.scene.addCan();

      // this.state.cans.push(data)
      // TODO: push new can to this.state.cans and drop it in the scene
    });
    this.el = createEl('div', { className: 'app' });
    addEl(this.el);
    this.preloader = createEl('div', { className: 'preloader' });
    this.preloaderBar = createEl('div', { className: 'preloader-bar' });
    this.preloaderProgress = createEl('span', { className: 'preloader-bar-progress' });
    addEl(this.preloaderBar, this.preloaderProgress);
    addEl(this.preloader, this.preloaderBar);
    addEl(this.el, this.preloader);
    // TODO: catch error here
    fetch('/api/getSignatures').then(res => res.json()).then((res) => {
      this.preloaderProgress.style.width = '10%';

      const shuffled = shuffle(res);
      // TODO: prepend a set list
      const double = JSON.parse(JSON.stringify(shuffled));
      const triple = JSON.parse(JSON.stringify(shuffled));
      const quad = JSON.parse(JSON.stringify(shuffled));

      const fullShuffled = shuffled.concat(double).concat(triple).concat(quad);// .concat(double).concat(triple).concat(quad);
      this.state.cans = fullShuffled;

      this.label = new Label();

      this.footer = new Footer(this.state);
      addEl(this.el, this.footer.el);

      this.viewContainer = createEl('div', { className: 'views' });
      addEl(this.el, this.viewContainer);

      this.views = {
        info: new InfoView(this.state),
        signature: new SignatureView(this.state, this.label),
        share: new ShareView(this.state),
      };

      addEl(this.viewContainer, this.views.info.el);
      addEl(this.viewContainer, this.views.signature.el);
      addEl(this.viewContainer, this.views.share.el);

      this.menu = new Menu(this.state);
      addEl(this.el, this.menu.el);

      this.instructions = new Instructions();
      addEl(this.el, this.instructions.el);

      this.scene = new Scene(this.state);
      addEl(this.el, this.scene.el);
      this.scene.el.addEventListener('progress', ({ detail }) => {
        this.preloaderProgress.style.width = `${detail.percent}%`;
        if (detail.percent === 100) {
          setTimeout(() => {
            this.preloader.classList.add('hidden');
          }, 1000);
        }
      });
      this.scene.el.addEventListener('loaded', () => {
        this.scene.setWrapTexture(this.label.canvasTexture);
        const path = window.location.pathname.substring(1);
        if (path === '') this.state.view = 'info';
        // if (path === 'signature' || path === 'start') this.state.view = path;
        if (path.indexOf('can') >= 0) {
          const index = parseInt(path.split('=')[1], 10);
          // TODO: this happens before cans have finished falling so height calc is off
          // this.state.can = index;
        }

        this.state.view = 'info'; // TODO: remove after dev
        // this.scene.resize();
      });
    });


    window.addEventListener('click', this.firstGesture);
  }

  firstGesture() {
    window.removeEventListener('click', this.firstGesture);
    this.state.firstGesture = true;
  }

  update(path, current, previous) {
    if (location.hostname === 'localhost' && !(path === 'cans' && previous.length === 0)) console.log(`${path}: ${previous} -> ${current}`);

    if (path === 'addingCans' && !current) {
      this.views.info.enable();
    }

    if (path === 'can') {
      this.state.view = current ? 'signature' : null;

      if (current) {
        // if (this.state.addingCans) return;

        this.scene.pickupCan(current);
        history.pushState({ page: `/can=${current}` }, '', `/can=${current}`);
      } else {
        history.pushState({ page: '/' }, '', '/');
      }
      if (previous) {
        this.scene.dropCan(previous);
      } else {
        // history.pushState({ page: '/' }, '/', '/');
      }
    }

    if (path === 'view') {
      if (this.views[previous]) this.views[previous].hide();
      if (this.views[current]) this.views[current].show();

      if (current) {
        this.instructions.hide();
        history.pushState({ page: current }, current, `/${current}`);
      } else {
        history.pushState({ page: '/' }, '/', '/');
      }
      this.menu[current ? 'hide' : 'show']();

      this.scene.centerCamera(!current, this.state.firstLoad);

      if (previous === 'info') {
        this.instructions.show();
      }

      this.state.firstLoad = false;
    }

    if (path === 'canSigned' && current) {
      this.state.canSigned = false;
      this.state.userID = this.state.can;
      this.state.can = null;
      this.menu.viewButton.classList.remove('hidden');
      this.menu.signButton.innerText = 'Sign Another';
      setTimeout(() => { this.scene.createAnimation(); }, 2000);
    }

    if (path === 'mute') {
      this.scene[current ? 'mute' : 'unmute']();
      this.footer[current ? 'mute' : 'unmute']();
    }

    if (path === 'animation') {
      this.views.share.updateAnimation(current);
    }
  }
}

const app = new App();
if (window.location.hostname === 'localhost') window.app = app;
