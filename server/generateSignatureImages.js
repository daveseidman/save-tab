const fs = require('fs');
const { createCanvas, registerFont } = require('canvas');

const fonts = [
  { name: 'AlexBrush Regular', file: 'AlexBrush-Regular.ttf' },
  { name: 'Autumn In November', file: 'Autumn-in-November.ttf' },
  { name: 'GreatVibes Regular', file: 'GreatVibes-Regular.ttf' },
  { name: 'Holligate Signature', file: 'Holligate-Signature.ttf' },
  { name: 'Southam Demo', file: 'Southam-Demo.otf' },
];
fonts.forEach(({ file, name }) => {
  registerFont(`server/fonts/${file}`, { family: name });
});

let progress = 0;

fs.readFile('./server/signatures-new.json', (err, data) => {
  const people = JSON.parse(data);
  const canvas = createCanvas(1024, 512);
  const context = canvas.getContext('2d');

  people.forEach((person) => {
    context.fillStyle = 'black';
    context.fillRect(0, 0, 1024, 512);
    const font = `${200}px ${fonts[Math.floor(Math.random() * fonts.length)].name}`;
    context.font = font;
    context.textAlign = 'center';
    context.fillStyle = 'white';
    context.fillText(person.name, 512, 256);
    const buffer = canvas.toBuffer('image/png');
    progress += 1;
    console.log((progress / people.length) * 100);

    fs.writeFile(`server/signatures/${person.name.replace(' ', '-')}.png`, buffer, () => {

    });
  });
});
