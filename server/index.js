require('dotenv').config();

const express = require('express');
const path = require('path');

const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const bodyParser = require('body-parser');
const fs = require('fs');
const fetch = require('isomorphic-fetch');


const captchaKey = '6LdO9cIeAAAAADiAvK_HqvuKDgiijRBeDNh78rid';


app.use(bodyParser.json());

app.use(express.static(`${__dirname}/../dist`)); // TODO: can probably use a wildcard here

app.use(['/start', '/signature', '/can=*'], (req, res) => {
  res.sendFile(path.resolve(__dirname, '..', 'dist', 'index.html'));
});

app.get('/api/getSignatures', (req, res) => {
  fs.readFile(`${__dirname}/signatures.json`, (err, data) => {
    res.send(JSON.parse(data));
  });
});

app.get('/api/signature/:signature', (req, res) => {
  // console.log('get image for', req.params.signature);
  res.sendFile(`${__dirname}/signatures/${req.params.signature}`);
});

app.post('/api/signature', (req, res) => {
  // TODO: path.join is more robust
  fs.readFile(`${__dirname}/signatures.json`, (err, data) => {
    const json = JSON.parse(data);

    const datestamp = new Date().toISOString().split('T').join('_').replace(/[\:\.]/g, '-');
    const { name, reason, location, signature, socket, token } = req.body;

    fetch(`https://www.google.com/recaptcha/api/siteverify?secret=${captchaKey}&response=${token}`, { method: 'post' })
      .then(captchaResJSON => captchaResJSON.json())
      .then((captchaRes) => {
        if (captchaRes.score >= 0.5) {
          const filename = `${name.replace(/\s+/g, '-')}.png`;
          const image = signature.replace(/^data:image\/\w+;base64,/, '');
          const buffer = Buffer.from(image, 'base64');
          fs.writeFile(`${__dirname}/signatures/${filename}`, buffer, () => {
            const newUser = { name, reason, location, signature: filename, socket };
            json.push(newUser);
            fs.writeFile(`${__dirname}/signatures.json`, JSON.stringify(json, null, 2), (err) => {
              if (!err) console.log(`Added Can: ${name}`);
              res.send({ success: !err, filename });
              io.of('/savetab').emit('canAdded', newUser);
            });
          });
        } else {
          res.send({ success: false });
        }
      })
      .catch((error) => {
        res.send({ success: false });
      });
  });
});

io.of('/savetab').on('connection', () => {
  // console.log('a user connected');
});
const server = http.listen(process.env.PORT, () => {
  console.log(`Listening on port ${server.address().port}`);
});
